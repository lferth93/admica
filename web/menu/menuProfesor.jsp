<%-- 
    Document   : menuProfesor
    Created on : 12/03/2015, 12:16:23 PM
    Author     : FernandoEsteban
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- Sidebar user panel -->
<div class="user-panel">
    <div class="pull-left image">
        <img src="../../../img/avatar3.png" class="img-circle" alt="User Image" />
    </div>
    <div class="pull-left info">
        <p>Hello, Jane</p>

        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
</div>
<!-- search form -->
<form action="#" method="get" class="sidebar-form">
    <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search..."/>
        <span class="input-group-btn">
            <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
        </span>
    </div>
</form>
<!-- /.search form -->
<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
    <li class="treeview">
        <a href="#">
            <i class="fa fa-check-square-o"></i>
            <span>Asistencia</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="charts/morris.html"><i class="fa fa-angle-double-right"></i> Pasar Lista</a></li>
            <li><a href="charts/flot.html"><i class="fa fa-angle-double-right"></i> Consultar Faltas Alumnos</a></li>
            <li><a href="charts/inline.html"><i class="fa fa-angle-double-right"></i> Mis Faltas</a></li>
            <li><a href="charts/inline.html"><i class="fa fa-angle-double-right"></i> Justificación Alumno</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-edit"></i>
            <span>Tareas</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="charts/morris.html"><i class="fa fa-angle-double-right"></i> Subir Rubrica</a></li>
            <li><a href="charts/flot.html"><i class="fa fa-angle-double-right"></i> Calificar</a></li>
            <li><a href="charts/inline.html"><i class="fa fa-angle-double-right"></i> Consulta</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-dot-circle-o"></i>
            <span>Libre Asignación</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="charts/morris.html"><i class="fa fa-angle-double-right"></i> Reservar</a></li>
            <li><a href="charts/flot.html"><i class="fa fa-angle-double-right"></i> Consulta</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-check-circle"></i>
            <span>Evaluación Docente</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="charts/morris.html"><i class="fa fa-angle-double-right"></i> Consulta</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-book"></i>
            <span>Biblioteca Virtual</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="charts/morris.html"><i class="fa fa-angle-double-right"></i> Mis archivos</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-table"></i>
            <span>Reportes</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="charts/morris.html"><i class="fa fa-angle-double-right"></i> Crear</a></li>
            <li><a href="charts/flot.html"><i class="fa fa-angle-double-right"></i> Consultar</a></li>
        </ul>
    </li>
</ul>
<%-- 
    Document   : menuRecursos Humanos
    Created on : 12/03/2015, 01:35:22 PM
    Author     : FernandoEsteban
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- Sidebar user panel -->
<div class="user-panel">
    <div class="pull-left image">
        <img src="../../../img/avatar3.png" class="img-circle" alt="User Image" />
    </div>
    <div class="pull-left info">
        <p>Hola, Usuario</p>

        <a href="#"><i class="fa fa-circle text-success"></i> En línea</a>
    </div>
</div>
<!-- search form -->
<form action="#" method="get" class="sidebar-form">
    <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Buscar..."/>
        <span class="input-group-btn">
            <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
        </span>
    </div>
</form>
<!-- /.search form -->
<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
    <li class="active">
        <a href="agregar.jsp">
            <i class="fa fa-plus"></i> 
            <span>Agregar Empleado</span>
        </a>
    </li>
    <li class="active">
        <a href="editar.jsp">
            <i class="fa fa-pencil"></i> 
            <span>Editar Empleado</span>
        </a>
    </li>
    <li class="active">
        <a href="borrar.jsp">
            <i class="fa fa-times"></i> 
            <span>Borrar Empleado</span>
        </a>
    </li>
    <li class="active">
        <a href="consultar.jsp">
            <i class="fa fa-search"></i> 
            <span>Consultar Empleados</span>
        </a>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-table"></i> <span>Reportes</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li>
                <a href="crearR.jsp">
                    <i class="fa fa-angle-double-right"></i>Crear
                </a>
            </li>
            <li>
                <a href="consultarR.jsp">
                    <i class="fa fa-angle-double-right"></i>Consultar
                </a>
            </li>
        </ul>
    </li>
</ul>
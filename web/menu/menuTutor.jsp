<%-- 
    Document   : menuTutor
    Created on : 12/03/2015, 03:00:23 PM
    Author     : FernandoEsteban
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- Sidebar user panel -->
<div class="user-panel">
    <div class="pull-left image">
        <img src="../../../img/avatar3.png" class="img-circle" alt="User Image" />
    </div>
    <div class="pull-left info">
        <p>Hola, Usuario</p>

        <a href="#"><i class="fa fa-circle text-success"></i> En línea</a>
    </div>
</div>
<!-- search form -->
<form action="#" method="get" class="sidebar-form">
    <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Buscar..."/>
        <span class="input-group-btn">
            <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
        </span>
    </div>
</form>
<!-- /.search form -->
<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
    <li class="treeview">
        <a href="#">
            <i class="fa fa-check-square-o"></i> 
            <span>Asistencia</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-edit"></i> <span>Tareas</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
    </li>
</ul>
<%-- 
    Document   : menuDirectorAcademico
    Created on : 12/03/2015, 01:10:22 PM
    Author     : FernandoEsteban
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- Sidebar user panel -->
<div class="user-panel">
    <div class="pull-left image">
        <img src="../../../img/avatar3.png" class="img-circle" alt="User Image" />
    </div>
    <div class="pull-left info">
        <p>Hello, Jane</p>

        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
</div>
<!-- search form -->
<form action="#" method="get" class="sidebar-form">
    <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search..."/>
        <span class="input-group-btn">
            <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
        </span>
    </div>
</form>
<!-- /.search form -->
<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
    <li class="treeview">
        <a href="#">
            <i class="fa fa-check-circle"></i>
            <span>Evaluación Docente</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="charts/morris.html"><i class="fa fa-angle-double-right"></i> Creación de Evaluación</a></li>
            <li><a href="charts/flot.html"><i class="fa fa-angle-double-right"></i> Resultados</a></li>
            <li><a href="charts/inline.html"><i class="fa fa-angle-double-right"></i> Editar</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-cogs"></i>
            <span>Asignación Docente</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="charts/morris.html"><i class="fa fa-angle-double-right"></i> Asignar Profesor</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-check-square-o"></i>
            <span>Asistencia</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="charts/morris.html"><i class="fa fa-angle-double-right"></i> Asistencia Profesor</a></li>
            <li><a href="charts/flot.html"><i class="fa fa-angle-double-right"></i> Justificación Profesor</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-table"></i>
            <span>Reportes</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="charts/morris.html"><i class="fa fa-angle-double-right"></i> Crear</a></li>
            <li><a href="charts/morris.html"><i class="fa fa-angle-double-right"></i> Consultar</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-dot-circle-o"></i>
            <span>Contraturno</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-angle-double-right"></i> Consulta</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-dot-circle-o"></i>
            <span>Libre Asignación</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="charts/morris.html"><i class="fa fa-angle-double-right"></i> Reservar</a></li>
            <li><a href="charts/flot.html"><i class="fa fa-angle-double-right"></i> Consulta</a></li>
        </ul>
    </li>
</ul>
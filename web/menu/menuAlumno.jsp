<%-- 
    Document   : menu
    Created on : 12/03/2015, 11:46:29 AM
    Author     : Mooze
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- Sidebar user panel -->
<div class="user-panel">
    <div class="pull-left image">
        <img src="../../../img/avatar3.png" class="img-circle" alt="User Image" />
    </div>
    <div class="pull-left info">
        <p>Hola, Usuario</p>

        <a href="#"><i class="fa fa-circle text-success"></i> En línea</a>
    </div>
</div>
<!-- search form -->
<form action="#" method="get" class="sidebar-form">
    <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Buscar..."/>
        <span class="input-group-btn">
            <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
        </span>
    </div>
</form>
<!-- /.search form -->
<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
    <li class="treeview">
        <a href="#">
            <i class="fa fa-check-square-o"></i> 
            <span>Asistencia</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-angle-double-right"></i> Consultar Faltas</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-edit"></i> <span>Tareas</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-angle-double-right"></i> Línea de tiempo</a></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i>Calificiones</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-dot-circle-o"></i>
            <span>Contraturno</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-angle-double-right"></i> Registrar Contraturno</a></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> Consulta</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-check-circle"></i>
            <span>Evaluación Docente</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-angle-double-right"></i> Evaluar</a></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> Consultar</a></li>
        </ul>
    </li>
    <li>
        <a href="#">
            <i class="fa fa-book"></i> <span>Biblioteca Virtual</span>
        </a>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-table"></i> <span>Reportes</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-angle-double-right"></i> Nuevo Reporte</a></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> Consultar Reportes</a></li>
        </ul>
    </li>
</ul>
<%-- 
    Document   : menu
    Created on : 12/03/2015, 11:46:29 AM
    Author     : Mooze
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- Sidebar user panel -->
<div class="user-panel">
    <div class="pull-left image">
        <img src="../../../img/avatar3.png" class="img-circle" alt="User Image" />
    </div>
    <div class="pull-left info">
        <p>Hello, Jane</p>

        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
</div>
<!-- search form -->
<form action="#" method="get" class="sidebar-form">
    <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search..."/>
        <span class="input-group-btn">
            <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
        </span>
    </div>
</form>
<!-- /.search form -->
<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
    <li class="treeview">
        <a href="#">
            <i class="fa fa-folder"></i>
            <span>Inventario</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="charts/morris.html"><i class="fa fa-angle-double-right"></i>Agregar</a></li>
            <li><a href="charts/flot.html"><i class="fa fa-angle-double-right"></i>Editar</a></li>
            
            <li><a href="charts/flot.html"><i class="fa fa-angle-double-right"></i>Consultar</a></li>
            <li><a href="charts/flot.html"><i class="fa fa-angle-double-right"></i>Borrar</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-paper-plane"></i>
            <span>Insumos</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="UI/general.html"><i class="fa fa-angle-double-right"></i>Control de Insumos</a></li>
        </ul>
    </li>
</ul>
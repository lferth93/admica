<%-- 
    Document   : index
    Created on : 12/03/2015, 02:34:34 PM
    Author     : LuisFernando
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Admica | Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />

        <link href="../../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

        <!-- Theme style -->
        <link href="../../../css/estilos.css" rel="stylesheet" type="text/css" />
        <!-- Estilos Admica -->
        <link href="../../../css/estilos-admica.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <%@include file="../../header.jsp"%>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <%@include file="../../../menu/menuRecursosHumanos.jsp" %>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Control de Empleados
                        <small>Consultar Empleado</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.jsp"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Consultar Empleado</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Lista de empleados</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="empleados" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Apellido Paterno</th>
                                        <th>Apellido Materno</th>
                                        <th>CURP</th>
                                        <th>Sexo</th>
                                        <th>Fecha de nacimiento</th>
                                        <th>Nivel de estudios</th>
                                        <th>Titulo</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Empleado 1</td>
                                        <td>AP1</td>
                                        <td>AM1</td>
                                        <td>HDJAG12363527hfjds2290</td>
                                        <td>Masculino</td>
                                        <td>23/05/1990</td>
                                        <td>Bachillerato</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Empleado 2</td>
                                        <td>AP2</td>
                                        <td>AM2</td>
                                        <td>35326253hfggs672362g</td>
                                        <td>Masculino</td>
                                        <td>23/05/1990</td>
                                        <td>Licenciatura</td>
                                        <td>Titulo 2</td>
                                    </tr>
                                    <tr>
                                        <td>Empleado 3</td>
                                        <td>AP3</td>
                                        <td>AM3</td>
                                        <td>J0JDKGJ5847383jghg</td>
                                        <td>Masculino</td>
                                        <td>03/02/1996</td>
                                        <td>Bachillerato</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Empleado 1</td>
                                        <td>AP1</td>
                                        <td>AM1</td>
                                        <td>HDJAG12363527hfjds2290</td>
                                        <td>Masculino</td>
                                        <td>23/05/1990</td>
                                        <td>Bachillerato</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Empleado 1</td>
                                        <td>AP1</td>
                                        <td>AM1</td>
                                        <td>HDJAG12363527hfjds2290</td>
                                        <td>Masculino</td>
                                        <td>23/05/1990</td>
                                        <td>Bachillerato</td>
                                        <td></td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Apellido Paterno</th>
                                        <th>Apellido Materno</th>
                                        <th>CURP</th>
                                        <th>Sexo</th>
                                        <th>Fecha de nacimiento</th>
                                        <th>Nivel de estudios</th>
                                        <th>Titulo</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js" type="text/javascript"></script>

        <script src="../../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="../../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="../../../js/AdminLTE/app.js" type="text/javascript"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="../../../js/AdminLTE/demo.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(function() {
                $('#empleados').dataTable();
            });
        </script>
    </body>
</html>
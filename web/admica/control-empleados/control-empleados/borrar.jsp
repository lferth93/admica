<%-- 
    Document   : index
    Created on : 12/03/2015, 02:34:34 PM
    Author     : FernandoEsteban
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Admica | Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="../../../css/estilos.css" rel="stylesheet" type="text/css" />
        <!-- Estilos Admica -->
        <link href="../../../css/estilos-admica.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <%@include file="../../header.jsp"%>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <%@include file="../../../menu/menuRecursosHumanos.jsp" %>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Control de Empleados
                        <small>Borrar Empleado</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.jsp"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Borrar Empleado</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- general form elements disabled -->
                        <div class="box box-admica-turquesa">
                            <div class="box-header">
                                <h3 class="box-title">Información del empleado</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <form role="form">
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label>Numero de Empleado</label>
                                            <input type="text" class="form-control" value="1827362" disabled/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label>Nombre</label>
                                            <input type="text" class="form-control" value="Ismael" placeholder="Nombre" disabled/>
                                            <label>Apellido paterno</label>
                                            <input type="text" class="form-control" value="Rosendo" placeholder="Apellido paterno" disabled/>
                                            <label>Apellido materno</label>
                                            <input type="text" class="form-control" value="Abarca" placeholder="Apellido materno" disabled/>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>CURP</label>
                                            <input type="text" class="form-control" value="ISRA901207HDBSNS19" placeholder="CURP" disabled/>
                                            <label>Sexo</label>
                                            <select class="form-control" disabled>
                                                <option selected>Masculino</option>
                                                <option >Femenino</option>
                                            </select>
                                            <label>Fecha de nacimiento</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input disabled type="text" class="form-control" value="1990/12/07" data-inputmask="'alias': 'YYYY/MM/DD'" data-mask/>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Nivel de estudios</label>
                                            <select class="form-control" id="nivel" disabled>
                                                <option value="1">Primaria</option>
                                                <option value="2">Secundaria</option>
                                                <option value="3">Bachillerato</option>
                                                <option value="4" selected>Licenciatura</option>
                                                <option value="5">Posgrado</option>
                                            </select> 
                                        </div>
                                        <div id="titulo" class="form-group col-md-6">
                                            <label>Titulo obtenido (solo licenciatura y porgrado)</label>
                                            <input type="text" class="form-control" value="Licenciado en informatica" placeholder="Titulo" disabled/>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-danger">Borrar empleado</button>
                            </div>
                        </div>
                    </div>
                </section>
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="../../../js/AdminLTE/app.js" type="text/javascript"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="../../../js/AdminLTE/demo.js" type="text/javascript"></script>
    </body>
</html>